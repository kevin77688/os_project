using namespace std;

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <vector>
#include <bits/stdc++.h>
#include <algorithm>
#include <string>

int REQUEST_TIME, ORIGINAL_HEAD_INDEX;
string MOVING_DIRECTION;

vector<int> copyVector(vector<int> & array){
    vector<int> result;
    for (int index = 0 ; index < array.size(); index++)
        result.push_back(array[index]);
    return result;
}

int fcfs(vector<int> inputRequests){
    int totalMovement = 0;
    int currentHead = ORIGINAL_HEAD_INDEX;
    vector<int> requests = copyVector(inputRequests);
    for(int index = 0; index < REQUEST_TIME; index++){
        int distance = abs(requests[index] - currentHead);
        currentHead = requests[index];
        totalMovement += distance;
    }
    return totalMovement;
}

int sstf(vector<int> inputRequests){
    int totalMovement = 0;
    int currentHead = ORIGINAL_HEAD_INDEX;
    vector<int> requests = copyVector(inputRequests);
    while(!requests.empty()){
        int minimumIndex = -1, minimumDistance = INT_MAX;
        for(int index = 0; index < requests.size(); index++){
            int distance = abs(requests[index] - currentHead);
            if (distance < minimumDistance){
                minimumDistance = distance;
                minimumIndex = index;
            }
        }
        currentHead = requests[minimumIndex];
        totalMovement += minimumDistance;
        requests.erase(requests.begin() + minimumIndex);
        requests.shrink_to_fit();
    }
    return totalMovement;
}

int scan(vector<int> inputRequests){
    int totalMovement = 0;
    int currentHead = ORIGINAL_HEAD_INDEX;
    vector<int> requests = copyVector(inputRequests);
    vector<int> leftRequest, rightRequest;
    string currentMovingDirection = MOVING_DIRECTION;
    // Split request by direction
    for(int request : requests)
        if (request <= ORIGINAL_HEAD_INDEX)
            leftRequest.push_back(request);
        else
            rightRequest.push_back(request);

    // Sort request
    sort(leftRequest.begin(), leftRequest.end());
    sort(rightRequest.begin(), rightRequest.end());
    reverse(rightRequest.begin(), rightRequest.end());

    while(!leftRequest.empty() || !rightRequest.empty()){
        if (leftRequest.empty())
            currentMovingDirection = "right";
        if (rightRequest.empty())
            currentMovingDirection = "left";
        if(currentMovingDirection.compare("left") == 0){
            totalMovement += abs(currentHead - leftRequest.back());
            currentHead = leftRequest.back();
            leftRequest.pop_back();
        }
        else if (currentMovingDirection.compare("right") == 0){
            totalMovement += abs(currentHead - rightRequest.back());
            currentHead = rightRequest.back();
            rightRequest.pop_back();
        }
    }
    return totalMovement;
}

int cscan(vector<int> inputRequests){
    int totalMovement = 0;
    int currentHead = ORIGINAL_HEAD_INDEX;
    vector<int> requests = copyVector(inputRequests);
    vector<int> leftRequest, rightRequest;
    for(int request : requests)
        if (request <= ORIGINAL_HEAD_INDEX)
            leftRequest.push_back(request);
        else
            rightRequest.push_back(request);
    
    // Sort requests
    sort(leftRequest.begin(), leftRequest.end());
    sort(rightRequest.begin(), rightRequest.end());
    reverse(leftRequest.begin(), leftRequest.end());
    reverse(rightRequest.begin(), rightRequest.end());

    // Move right
    while(!rightRequest.empty()){
        totalMovement += abs(rightRequest.back() - currentHead);
        currentHead = rightRequest.back();
        rightRequest.pop_back();
    }
    while(!leftRequest.empty()){
        totalMovement += abs(leftRequest.back() - currentHead);
        currentHead = leftRequest.back();
        leftRequest.pop_back();
    }

    return totalMovement;
}

int main(int argc, char*argv[]){
    vector<int> requests;

    if (argc == 4){
        REQUEST_TIME = atoi(argv[1]);
        ORIGINAL_HEAD_INDEX = atoi(argv[2]);
        MOVING_DIRECTION = argv[3];

    }else{
        cout << "Enter the request times : ";
        cin >> REQUEST_TIME;
        cout << "Enter the original disk head index (0 ~ 4999) : ";
        cin >> ORIGINAL_HEAD_INDEX;
        cout << "Enter the current moving direction (l, r) : ";
        char direction;
        cin >> direction;
        switch(direction){
            case 'l':
                MOVING_DIRECTION = "left";
                break;
            case 'r':
                MOVING_DIRECTION = "right";
                break;
            default:
                cout << "Direction enter error ! Please try again." << endl;
                exit(1);
        }
    }

    if (ORIGINAL_HEAD_INDEX > 4999){
        cout << "Disk head index error ! " << endl << "Please try again" << endl;
        exit(1);
    }

    cout << endl;
    cout << "The program will request " << REQUEST_TIME << " times." << endl;
    cout << "And the initial disk head is at index = " << ORIGINAL_HEAD_INDEX << "." << endl;
    cout << "The disk is moving \"" << MOVING_DIRECTION << "\" now." << endl;

    // Set random request
    srand(time(nullptr));
    for(int index = 0; index < REQUEST_TIME; index++)
        requests.push_back(rand() % 5000);

    cout << endl;
    cout << " FCFS  head movements : " << fcfs(requests)  << endl;
    cout << " SSTF  head movements : " << sstf(requests)  << endl;
    cout << " SCAN  head movements : " << scan(requests)  << endl;
    cout << "C-SCAN head movements : " << cscan(requests) << endl;

    return 0;
}
