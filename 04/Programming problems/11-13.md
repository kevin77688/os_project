# 11-13 Problems
## (1) Hard link -> file1.txt 
- Screenshot  <br>  
![](11_13_Image/11_13-1.png)
- file1.txt 與 file2.txt的 inode 皆為 550106
- 並且兩者的內容完全相同

## (2) Remove file and strace
- Screenshot  <br>  
![](11_13_Image/11_13-2.png)
- file1.txt 與 file2.txt 的內容是相同的
- 刪除 file1.txt 之後，file2.txt 仍然存在
- linux 使用 /bin/rm 的 system calls

## (3) Soft link file3.txt
- Screenshot  <br>
![](11_13_Image/11_13-3.png)
- file3.txt 的 inode 為 550175， file4.txt 的 inode 為550177，兩者並不同
- 編輯 file4.txt 之後，file3.txt 的內容也會被改變
- 當刪除 file3.txt 之後，file4.txt 的連結就斷掉了，當去更新內容的時候，linux會當作完全新的檔案在編輯。