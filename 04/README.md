# Homework 04

## 開發環境
 - Linux

## 編譯工具
- g++
- CMake

## 編譯方法
 - 內建 Makefile，在Programming ploblems資料夾中直接 `make` 即可執行
## Detail
 - 本次作業只有Programming problems，內容在資料夾內，截圖的圖片會放於 Screenshot 的資料夾中
 - 執行方法 ： 所有編譯過的檔案會在bin資料夾中 並且需依照題目格式執行
 - Programming problems : 
    - 11_13 : 本題為操作問答題，內容會放在11-13.md當中
        - [11-13.md](./Programming%20problems/11-13.md)
    - 12_16 : 程式題 12.16
        - 可直接執行
            - Usage : `bin/12_16`
        - 也可以輸入三個參數來運算
            - Usage : `bin/12_16 [request time (int)] [disk head index (int)] ["left"/"right"]`
            - Example : `bin/12_16 5000 1000 left`
## Contact
 - 電資三 段凱文 106820007
 - 信箱 : kevin77688@gmail.com


