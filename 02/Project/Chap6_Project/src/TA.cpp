using namespace std;

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <unistd.h>
#include <semaphore.h>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <thread>

enum state {Asking, Waiting, AskChair};
class monitor();
monitor _globalMonitor;

class Student{
public:
    Student(int id){
        _id = id;
        cout << "Student " << id  <<" is ready." << endl;
        _currentState = AskChair;
        while(_currentState == AskChair){
            
        }
    }

    state getState(){
        return _currentState;
    }

    void ask(){
        sleep((rand() % 10) + 1);
    }
private:
    int _id;
    state _currentState;
};

class monitor{
public:
    monitor(){
        chairSemaphore = 3;
    }
    void checkChair(Student s){
        if (s.getState() == Waiting && chairSemaphore > 0){
            chairSemaphore--;

        }

    }
private:
    int chairSemaphore;
    vector<Student> chair;
};

void createStudent(int id){
    Student student(id);

}

int main(){
    int numberOfStudent;
    vector<thread> studentThread;
    srand((unsigned)time(0));
    cout << "Enter the number of student wanna to ask question : ";
    cin >> numberOfStudent;
    for (int i = 1; i <= numberOfStudent; i++){
        studentThread.push_back(thread(createStudent, i));
    }

    for (auto &i : studentThread){
        i.join();
    }
    return 0;
}