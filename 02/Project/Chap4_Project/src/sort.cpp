using namespace std;

#include <iostream>
#include <sstream>
#include <thread>
#include <vector>

void splitVector(vector<int> & firstVector, vector<int> & secondVector, stringstream & ss){
    char checkChar;
    bool splitEquals = true;
    while(true){
        int number;
        ss >> number;
        if (splitEquals)
            firstVector.push_back(number);
        else
            secondVector.push_back(number);
        splitEquals = !splitEquals;
        ss >> checkChar;
        if (checkChar == ']')
            break;
        else if (checkChar != ',')
            throw string("Input error !");
    }
}

void swap(int *xp, int *yp)  
{  
    int temp = *xp;  
    *xp = *yp;  
    *yp = temp;  
}  

void sortVector(int * arr, int n){
    int i, j;  
    for (i = 0; i < n-1; i++)
      
    // Last i elements are already in place  
    for (j = 0; j < n-i-1; j++)  
        if (arr[j] > arr[j+1])  
            swap(&arr[j], &arr[j+1]);  
}

void mergeArray(int * firstArray, int sizeOfFirstArray, int * secondArray, int sizeOfSecondArray, int * mergeArray){
    int firstArrayIndex = 0, secondArrayIndex = 0;
    int totalSize = sizeOfFirstArray + sizeOfSecondArray;
    for (int index = 0; index < totalSize; index++){
        int firstNumber = (firstArrayIndex < sizeOfFirstArray) ? firstArray[firstArrayIndex] : 999999;
        int secondNumber = (secondArrayIndex < sizeOfSecondArray) ? secondArray[secondArrayIndex] : 999999;
        if (firstNumber > secondNumber){
            mergeArray[index] = secondNumber;
            secondArrayIndex++;
        }else{
            mergeArray[index] = firstNumber;
            firstArrayIndex++;
        }
    }
}

void sorting(string & s){
    char checkChar;
    vector<int> firstVector, secondVector;
    stringstream ss(s);
    ss >> checkChar;
    if (checkChar != '[')
        throw string("Input error !");
    splitVector(firstVector, secondVector, ss);
    int * firstArray = &firstVector[0];
    int * secondArray = &secondVector[0];
    int totalSize = firstVector.size() + secondVector.size();
    int * resultArray = new int[totalSize];
    thread firstThread = thread(sortVector, firstArray, firstVector.size());
    thread secondThread = thread(sortVector, secondArray, secondVector.size());
    firstThread.join();
    secondThread.join();
    thread mergeThread = thread(mergeArray, firstArray, firstVector.size(), secondArray, secondVector.size(), resultArray);
    mergeThread.join();
    
    cout << "Sorted array : [";
    for (int index = 0; index < totalSize - 1; index++)
        cout << resultArray[index] << ", ";
    cout << resultArray[totalSize - 1] << "]" << endl;
}

int main(){
    try{
        string inputString;
        cout << "Input an array in the following format \"[a1, a2, ..., an]\" : ";
        getline(cin, inputString);
        cout << "Your input : " << inputString << endl;
        sorting(inputString);
    }catch (string s){
        cout << s << endl;
    }
    return 0;
}
