using namespace std;
#include <iostream>
#include <thread>
#include <mutex>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <vector>

mutex mtx;
int pointInCircle = 0;

void checkPointInCircle(int numberOfPoint){
    int numberOfPointInCircle = 0;
    for (int index = 0; index < numberOfPoint; index++){
        double randomX = (((double)rand() / (RAND_MAX)) * 2) - 1;
        double randomY = (((double)rand() / (RAND_MAX)) * 2) - 1;
        if ((pow(randomX, 2) + pow(randomY, 2)) < 1)
            numberOfPointInCircle += 1;
    }
    if (numberOfPointInCircle > 0){
        mtx.lock();
        pointInCircle += numberOfPointInCircle;
        mtx.unlock();
    }
}

int main(){
    int numberOfPoint;
    int numberOfThread;
    int pointPerThread;
    vector<thread> listOfThread;

    cout << "Enter the number of point : ";
    cin >> numberOfPoint;
    cout << "Enter the number of thread to split : ";
    cin >> numberOfThread;
    if (numberOfThread > numberOfPoint)
        cout << "Too much thread !" << endl << "The program will uses one point per thread" << endl;
    pointPerThread = ceil(numberOfPoint / numberOfThread);
    numberOfPoint = (int)(numberOfThread * pointPerThread);

    /// set random seed 
    srand((unsigned)time(0));

    for (int index = 0; index < numberOfThread; index++)
        listOfThread.push_back(thread(checkPointInCircle, pointPerThread));
    for (thread & th : listOfThread)
        th.join();

    double pi = (4 * (double)pointInCircle / (double)numberOfPoint);
    cout << "π = " << pi << endl;
    return 0;
}