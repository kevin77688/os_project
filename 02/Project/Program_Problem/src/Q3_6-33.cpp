using namespace std;
#include <iostream>
#include <thread>
#include <mutex>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <vector>

mutex mtx;
int pointInCircle = 0;

void checkPointInCircle(){
    int numberOfPointInCircle = 0;
    double randomX = (((double)rand() / (RAND_MAX)) * 2) - 1;
    double randomY = (((double)rand() / (RAND_MAX)) * 2) - 1;
    if ((pow(randomX, 2) + pow(randomY, 2)) < 1){
        mtx.lock();
        pointInCircle++;
        mtx.unlock();
    }
}

int main(){
    int numberOfPoint;
    vector<thread> listOfThread;

    cout << "Enter the number of point : ";
    cin >> numberOfPoint;

    /// set random seed 
    srand((unsigned)time(0));

    for (int index = 0; index < numberOfPoint; index++)
        listOfThread.push_back(thread(checkPointInCircle));
    for (thread & th : listOfThread)
        th.join();

    double pi = (4 * (double)pointInCircle / (double)numberOfPoint);
    cout << "π = " << pi << endl;
    return 0;
}