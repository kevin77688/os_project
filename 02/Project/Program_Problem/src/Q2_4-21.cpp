using namespace std;

#include <iostream>
#include <thread>
#include <vector>

int *fibonacci;

void Calculate_Fibonacci(int round){
    for (int index = 2; index < round; index++)
        fibonacci[index] = fibonacci[index - 1] + fibonacci[index - 2];
}


int main(){
    int numberOfFib;
    cout << "Enter the number to generate the sequence : ";
    cin >> numberOfFib;
    if (numberOfFib < 2){
        cout << "Input error !" << endl;
        return 1;
    }
    fibonacci = new int[numberOfFib];
    fibonacci[0] = 0;
    fibonacci[1] = 1;
    thread fib = thread(Calculate_Fibonacci, numberOfFib);
    fib.join();

    cout << "Fibonacci number : ";
    for (int index = 0; index < numberOfFib; index++)
        cout << fibonacci[index] << ", ";
    cout << endl;
    return 0;
}