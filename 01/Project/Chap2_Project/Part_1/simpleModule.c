#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>

int simple_init(void){
    printk(KERN_INFO "Locaing Moudle\n");

    return 0;
}

void simple_exit(void){
    printk(KERN_INFO "Removing Module\n");
}

module_init(simple_init);
module_exit(simple_exit);

MODULE_LICENSE("License");
MODULE_DESCRIPTION("Simple Module test");
MODULE_AUTHOR("t106820007 Kevin");