#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/slab.h>


struct birthday {
    char *name;
    int year;
    int month;
    int day;
    struct list_head head;
}birthday;

LIST_HEAD(birthdayList);

int birthdayListInitial(void){
    
    struct birthday *person;
    struct birthday *visitIndex;

    printk(KERN_INFO "Creating link list : birthday\n");

    person = kmalloc(sizeof(*person), GFP_KERNEL);
    person -> name = "First";
    person -> year = 2000;
    person -> month = 01;
    person -> day = 02;
    INIT_LIST_HEAD(&person -> head);
    list_add_tail(&person -> head, &birthdayList);

    person = kmalloc(sizeof(*person), GFP_KERNEL);
    person -> name = "Second";
    person -> year = 2001;
    person -> month = 02;
    person -> day = 05;
    INIT_LIST_HEAD(&person -> head);
    list_add_tail(&person -> head, &birthdayList);

    person = kmalloc(sizeof(*person), GFP_KERNEL);
    person -> name = "Third";
    person -> year = 2003;
    person -> month = 07;
    person -> day = 15;
    INIT_LIST_HEAD(&person -> head);
    list_add_tail(&person -> head, &birthdayList);

    person = kmalloc(sizeof(*person), GFP_KERNEL);
    person -> name = "Fourth";
    person -> year = 2002;
    person -> month = 11;
    person -> day = 29;
    INIT_LIST_HEAD(&person -> head);
    list_add_tail(&person -> head, &birthdayList);

    person = kmalloc(sizeof(*person), GFP_KERNEL);
    person -> name = "Fifth";
    person -> year = 1999;
    person -> month = 12;
    person -> day = 01;
    INIT_LIST_HEAD(&person -> head);
    list_add_tail(&person -> head, &birthdayList);


    printk(KERN_INFO "Visit each node : \n");
    list_for_each_entry(visitIndex, &birthdayList, head){
        printk (KERN_INFO "Birthday Name : %s", visitIndex -> name);
        printk (KERN_INFO "\tYear : %d\n", visitIndex -> year);
        printk (KERN_INFO "\tMonth : %d\n", visitIndex -> month);
        printk (KERN_INFO "\tDay : %d\n\n", visitIndex -> day);
    }

    printk("Visit node finished\n\n");

    return 0;
}

void birthdayListExit(void){


    struct birthday *index;
    struct birthday *nextNode;
    printk(KERN_INFO "Removing link list : birthday");
    
    list_for_each_entry_safe(index, nextNode, &birthdayList, head){
        printk(KERN_INFO "Deleting Name : %s \n", index -> name);
        printk(KERN_INFO "\tYear : %d\n", index -> year);
        printk(KERN_INFO "\tMonth : %d\n", index -> month);
        printk(KERN_INFO "\tDay : %d\n", index -> day);

        list_del(&index -> head);
        kfree(index);
    }

    printk(KERN_INFO "All birthday list cleared !\n");
}

module_init(birthdayListInitial);
module_exit(birthdayListExit);

MODULE_LICENSE ("License");
MODULE_DESCRIPTION ("Birthday list ~~~");
MODULE_AUTHOR("t106820007 Kevin");