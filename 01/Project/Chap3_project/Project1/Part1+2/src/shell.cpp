using namespace std;

#include <iostream>
#include <vector>
#include <unistd.h>

void executeCommand(char* command){
    pid_t pid = fork();
    if (pid < 0) {
        cout << "Create child process failed !" << endl;
        exit(1);
    }
    else if (pid == 0)
        system(command);
}

void showHistory(vector<string> &history){
    int historySize = history.size();
            for (int index = 0; index < historySize; index++)
                cout << index + 1 << ".\t" << history.at(index) << endl;
}


int main(){
    char userInput[100];
    string userInputString;
    vector<string> history;

    while(true){
        cout << "shell >> ";
        cin.getline(userInput, 100);
        userInputString = string(userInput);

        if (userInputString == "history")
            showHistory(history);
        else if (userInputString == "exit")
            break;
        else{
            history.insert(history.begin(), userInput);
            system(userInput);
        }
    }
    return 0;
}