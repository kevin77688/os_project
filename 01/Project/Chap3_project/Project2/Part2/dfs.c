#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sched/signal.h>

void dfs(struct task_struct *task){
    struct task_struct *nextTask;
    struct list_head *list;

    list_for_each(list, &task -> children){
        nextTask = list_entry(list, struct task_struct, sibling);
        printk(KERN_INFO "Process name : %s\n", nextTask -> comm);
        printk(KERN_INFO "\tProcess PID : %d\n", nextTask -> pid);
        printk(KERN_INFO "\tProcess state : %ld\n", nextTask -> state);
        dfs(nextTask);
    }
}

int dfsTask_init(void){
    
    printk(KERN_INFO "Creating dfs task module\n");
    dfs(&init_task);

    printk(KERN_INFO "Module loaded\n");
    return 0;
}

void dfsTask_exit(void){
    printk(KERN_INFO "Module removed\n");
}

module_init(dfsTask_init);
module_exit(dfsTask_exit);

MODULE_LICENSE("License");
MODULE_DESCRIPTION("dfs find task");
MODULE_AUTHOR("t106820007 Kevin");