#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sched/signal.h>

int linearTask_init(void){
    struct task_struct *task;

    printk(KERN_INFO "Creating linear task module\n");

    for_each_process(task){
        printk(KERN_INFO "Process name : %s\n", task -> comm);
        printk(KERN_INFO "\tProcess PID : %d\n", task -> pid);
        printk(KERN_INFO "\tProcess state : %ld\n", task -> state);
    }
        
    return 0;
}

void linearTask_exit(void){
    printk(KERN_INFO "Linear task module removed\n");
}

module_init(linearTask_init);
module_exit(linearTask_exit);

MODULE_LICENSE("License");
MODULE_DESCRIPTION("Linear find task");
MODULE_AUTHOR("t106820007 Kevin");