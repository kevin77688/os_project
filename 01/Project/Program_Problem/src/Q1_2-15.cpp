using namespace std;

#include <iostream>
#include <fstream>
int main(){

        fstream inputFile, outputFile;
        string inputFileString, outputFileString;
        char currentCopy;

        cout << "Please enter the input file name : ";
        cin >> inputFileString;
        inputFile.open(inputFileString, fstream::in);
        if (inputFile.fail()) {
                cout << "the input file cannot be opened !" << endl;
                return 1;
        }

        cout << "Please enter the output file name : ";
        cin >> outputFileString;
        outputFile.open(outputFileString, fstream::out | fstream::trunc);

        while (!inputFile.eof()) {
                inputFile.get(currentCopy);
                outputFile.put(currentCopy);
        }
        inputFile.close();
        outputFile.close();

        cout << "Copy file completed !" << endl;
        return 0;
}
