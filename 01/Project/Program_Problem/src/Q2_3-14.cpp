using namespace std;

#include <iostream>
#include <unistd.h>
#include <sys/wait.h>

int main(){

        int inputNumber;

        cout << "Please enter the number : ";
        cin >> inputNumber;
        if (cin.fail()) {
                cout << "Input format error !" <<endl;
                return 1;
        }

        if (inputNumber < 1) {
                cout << "Input number cannot below 2 !!" << endl;
                return 1;
        }

        pid_t pid = fork();
        if (pid < 0) {
                cout << "Create child process failed !" << endl;
                return 1;
        }
        else if (pid == 0) {
                cout << "Child process in PID = " << getpid() << endl;
                cout << "Starting the sequence : " << inputNumber;
                int currentNumber = inputNumber;
                while (currentNumber != 1) {
                        currentNumber = (currentNumber % 2) ? (3 * currentNumber + 1) : (currentNumber / 2);
                        cout << ", " <<  currentNumber;
                }
                cout << endl;
                cout << "Child process terminated !" << endl;
        }
        else if (pid > 0) {
                cout << "Parent process in PID = " << getpid() << endl;
                wait(NULL);
        }

        cout << "Parent process terminated !" << endl;

        return 0;
}
