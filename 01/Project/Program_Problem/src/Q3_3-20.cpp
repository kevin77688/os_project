using namespace std;

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char *argv[]){

        string inputFileString, outputFileString;
        int pipes[2]; // pipes[0] = read, pipes[1] = write
        char readText[256], writeText[256];
        ssize_t charMemory;

        if (argc != 3) {
                cout << "Input form error !" << endl;
                return 1;
        }

        inputFileString = argv[1];
        outputFileString = argv[2];

        pipe(pipes);
        if (pipe(pipes) == -1) {
                cout << "Create pipe failed !" << endl;
                return 1;
        }
        switch (fork()) {
        case -1:
                cout << "Create child process error !" << endl;
                return 1;
        case 0:
                close(pipes[1]);
                charMemory = read(pipes[0], writeText, sizeof(writeText));
                close(pipes[0]);
                write(open(outputFileString.c_str(), O_CREAT | O_WRONLY), writeText, charMemory);
                break;
        default:
                close(pipes[0]);
                charMemory = read(open(inputFileString.c_str(), O_RDONLY), readText, sizeof(readText));
                write(pipes[1], readText, charMemory);
                close(pipes[1]);
                break;
        }
}
