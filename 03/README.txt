開發環境 : Linux
編譯工具 ： g++, cmake
編譯方法 ： 在檔案位置直接make

前三題的Programming Problems 會在Program_Problem資料夾中
截圖的圖片會放於 Screenshot 的資料夾中
執行方法 : 所有編譯過的檔案會在bin資料夾中 並且需依照題目格式執行
Programming problems : 
    7_17    可直接執行
            Usage : bin/7_17

    8_25    可以不輸入參數直接執行，將以測試資料 (19986) 執行
            Usage : bin/8_25
            也可以輸入一個參數來運算
            Usage : bin/8_25 [number]

    9_26    可以直接執行並依內容輸入參數
            Usage : bin/9_26
            也可以輸入兩個參數來執行
            Usage : bin/9_26 "[n1, n2, ..., nk]" [frameSize]

Programming project ： 
    Chap07  可以依照內容輸入執行
            Usage : bin/Chap07
            也可以輸入兩個參數來執行
            Usage : bin/Chap07 [Customer count] [Res1, Res2, ...,ResN]

    Chap09  可以直接執行，將使用測試資料位於 "data/addresses.txt" 作為測試資料
            Usage : bin/Chap09
            也可以將自己的檔案作為參數輸入之，輸入檔案為隨機記憶體存取位置 [ 0 ~ 65535 ]
            Usage : bin/Chap09 [FilePath]

電資三 段凱文 106820007
信箱 : kevin77688@gmail.com
