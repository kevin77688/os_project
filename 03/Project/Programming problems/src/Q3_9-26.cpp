using namespace std;

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <bits/stdc++.h> 
#include <map>

int totalPage;
vector<int> pageSequence;
int* pages;

void stringToPageArray(string input){
    stringstream stream(input);
    char c;
    stream >> c;
    do{
        int buffer;
        stream >> buffer >> c;
        pageSequence.push_back(buffer);
    } while (c == ',');
}

vector<int> copySequence(){
    vector<int> copyVector;
    for (int i : pageSequence)
        copyVector.push_back(i);
    return copyVector;
}

void initialPages(){
    pages = new int[totalPage];
    for (int index = 0; index < totalPage; index++)
        pages[index] = -1;
}

int checkContainIndex(int number){
    for (int index = 0; index < totalPage; index++)
        if (pages[index] == number)
            return index;
    return -1;
}

void printPages(int page){
    cout << "Check page " << page << " : ";
    for (int index = 0; index < totalPage; index++)
        cout << "\t" << pages[index];
    cout << endl;
}

void moveElementToLast(vector<int> & v, int index){
    int temp = v[index];
    v.erase(v.begin() + index);
    v.push_back(temp);
    v.shrink_to_fit();
}

int getVectorIndex(vector<int> & v, int element){
    for (int i = 0; i < v.size(); i++)
        if (v.at(i) == element)
            return i;
}

int getFarthestPageIndex(int index, vector<int> & checkPoint){
    vector<int> sequence = copySequence();
    map<int, int> result;

    for(int i = 0; i < checkPoint.size(); i++)
        result[pages[checkPoint.at(i)]] = INT_MAX;

    for (int i = pageSequence.size() - 1; i >= index; i--)
        result[sequence.at(i)] = i;

    int currentCheckElement = -1;
    int returnIndex = -1;
    for(int j = 0; j < totalPage; j++){
        if (result[pages[j]] == currentCheckElement){
                if (getVectorIndex(checkPoint, j) < getVectorIndex(checkPoint, returnIndex) && pages[j] != -1){
                    currentCheckElement = result[pages[j]];
                    returnIndex = j;
                }
        }
        else if (result[pages[j]] > currentCheckElement){
            currentCheckElement = result[pages[j]];
            returnIndex = j;
        }
    }
    return returnIndex;
}

void FIFO(){
    vector<int> sequence = copySequence();
    initialPages();
    int currentIndex = -1;
    int pageFault = 0;
    for (int i : sequence){
        if (checkContainIndex(i) == -1){
            currentIndex = (currentIndex + 1) % totalPage;
            pages[currentIndex] = i;
            pageFault++;
        }
        printPages(i);
    }
    cout << "Total page fault : " << pageFault << endl;
    delete[] pages;
}

void LRU(){
    vector<int> sequence = copySequence();
    initialPages();
    int pageFault = 0;
    vector<int> nextSwapPages;
    for (int index = 0 ; index < totalPage; index++)
        nextSwapPages.push_back(index);
    for (int i : sequence){
        int checkIndex = checkContainIndex(i);
        if (checkIndex != -1){
            moveElementToLast(nextSwapPages, getVectorIndex(nextSwapPages, checkIndex));
        }
        else{
            pages[nextSwapPages[0]] = i;
            moveElementToLast(nextSwapPages, 0);
            pageFault++;
        }
        printPages(i);
    }
    cout << "Total page fault : " << pageFault << endl;
    delete[] pages;
}

void Optimal(){
    vector<int> sequence = copySequence();
    initialPages();
    int pageFault = 0;
    int currentIndex = 0;
    vector<int> nextSwapPages;
    for (int index = 0 ; index < totalPage; index++)
        nextSwapPages.push_back(index);
    for(int i : sequence){
        int checkIndex = checkContainIndex(i);
        if (checkIndex != -1)
            moveElementToLast(nextSwapPages, getVectorIndex(nextSwapPages, checkIndex));
        else{
            pageFault++;
            int index = getFarthestPageIndex(currentIndex, nextSwapPages);
            pages[index] = i;
            moveElementToLast(nextSwapPages, getVectorIndex(nextSwapPages, index));
        }
        currentIndex++;
        printPages(i);
    }
    cout << "Total page fault : " << pageFault << endl;
    delete[] pages;
}

int main(int argc, char* argv[]){
    string inputRandomPage;
    if (argc == 1){
        cout << "Enter the random page-reference string in the following format ";
        cout << "\"[p1, p2, p3, ..., pn]\" : ";
        getline(cin, inputRandomPage);

        cout << "Enter the frame size : ";
        cin >> totalPage;
    }
    else{
        inputRandomPage = argv[1];
        totalPage = atoi(argv[2]);
    }
    stringToPageArray(inputRandomPage);

    while (true){
        int choise;

        cout << endl << "Choose the algorithm to run the page-replacement" << endl;
        cout << "Enter \'1\' select FIFO" << endl;
        cout << "Enter \'2\' select LRU" << endl;
        cout << "Enter \'3\' select Optimal" << endl;
        cout << "Enter \'4\' to Exit" << endl;
        cout << endl << "Your choise : ";
        cin >> choise;

        switch(choise){
            case 1:
                FIFO();
                break;
            case 2:
                LRU();
                break;
            case 3:
                Optimal();
                break;
            case 4:
                return 0;
            default:
                cout << "Input error ! Please try again" << endl;
                break;
        }
    }
    return 0;
}