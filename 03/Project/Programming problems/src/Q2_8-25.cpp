using namespace std;

#include <iostream>
#include <errno.h>
#include <limits.h>

int PAGE_SIZE = 4096;

int main(int argc, char* argv[]){

    long inputNumber;

    if (argc > 2){
        cout << "Input argument error !" << endl;
        cout << "There should be only one argument in the program" << endl;
        return 1;
    }
    else if (argc == 1){
        cout << "Missing argument !" << endl;
        cout << "Using default number 19986 as test case" << endl;
        inputNumber = 19986;
    }
    else if (argc == 2){
        inputNumber = strtol(argv[1], 0, 10);
        if (errno == ERANGE){
            if (inputNumber == LONG_MIN)
                cout << "Input underflow ~ ~ ~" << endl;
            else if (inputNumber == LONG_MAX)
                cout << "Input overflow ~ ~ ~" << endl;
            else
                cout << "Program error !";
            return 1;
        }
    }

    int page_number = (inputNumber / PAGE_SIZE);
    int offset = inputNumber % PAGE_SIZE;
    cout << endl;
    cout << "The address " << inputNumber << " contains:" << endl;
    cout << "page number = " << page_number << endl;
    cout << "offset = " << offset << endl;

    return 0;
}