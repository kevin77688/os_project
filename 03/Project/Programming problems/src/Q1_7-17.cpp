using namespace std;

#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <random>
#include <ctime>
#include <unistd.h>
#include <mutex>

mutex mtx;
bool allCreate;

void passBridge(string location, int number){
    mtx.lock();
    string prepare = location + " farmer " + to_string(number) + " is passing the bridge \n";
    cout << prepare;
    sleep(rand() % 2);
    string end = location + " farmer " + to_string(number) + " has left the bridge \n\n";
    cout << end;
    mtx.unlock();
}

void createFarmer(string location, int number){
    string s = location + " farmer " + to_string(number) + " has created \n";
    cout << s;
    while(!allCreate)
        ;;
    passBridge(location, number);
}

int main(){
    int numberOfNorth, numberOfSouth;
    vector<thread> allFarmers;
    allCreate = false;
    srand(time(NULL));

    cout << "Enter the number of North farmers : ";
    cin >> numberOfNorth;
    cout << "Enter the number of South farmers : ";
    cin >> numberOfSouth;
    // numberOfNorth = 10;
    // numberOfSouth = 8;

    for (int index = 0; index < numberOfNorth; index++)
        allFarmers.push_back(thread(createFarmer, "North", index));
    for (int index = 0; index < numberOfSouth; index++)
        allFarmers.push_back(thread(createFarmer, "South", index));
    sleep(1);
    allCreate = true;

    for (thread &t : allFarmers)
        if (t.joinable())
            t.join();


    return 0;
}