using namespace std;

#define PAGE_MASK       0xFF00
#define OFFSET_MASK     0xFF
#define FRAME_SIZE      256
#define FRAME_COUNT     256
#define TLB_SIZE        16
#define PAGE_TABLE_SIZE 256
#define PAGE_SIZE       256

#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>
#include <vector>

int TLB_Hits = 0;
int pageFault = 0;
int availableFrame = 0;
int physicalMemory[FRAME_COUNT][FRAME_SIZE];
bool printAddress;
map<int, int> TLB;
map<int, int> pageTable;    //<PGNum, PGFrame>
vector<int> TLB_PageNumber;
ifstream backingStore;

void readFromBackingStore(int pageNumber){
    char buffer[PAGE_SIZE];
    backingStore.seekg(pageNumber * PAGE_SIZE);
    backingStore.read(buffer, PAGE_SIZE);
    for (int i = 0; i < PAGE_SIZE; i++)
        physicalMemory[availableFrame][i] = buffer[i];
    pageTable[pageNumber] = availableFrame;

    availableFrame++;
}

void addToTLB(int pageNumber, int frameNumber){
    auto it = TLB.find(pageNumber);
    if (it != TLB.end())
        TLB.erase(it);

    if (TLB.size() > TLB_SIZE) {
        TLB.erase(TLB.find(TLB_PageNumber.at(0)));
        TLB_PageNumber.erase(TLB_PageNumber.begin());
    }

    for (int i = 0; i < TLB_PageNumber.size(); i++)
        if (TLB_PageNumber.at(i) == pageNumber)
            TLB_PageNumber.erase(TLB_PageNumber.begin() + i);
    TLB_PageNumber.shrink_to_fit();

    TLB[pageNumber] = frameNumber;
    TLB_PageNumber.push_back(pageNumber);
}

void getPage(int logicalAddress){
    int frameNumber = -1;

    int pageNumber = ((logicalAddress & PAGE_MASK) >> 8);
    int offset = (logicalAddress & OFFSET_MASK);
    if(TLB.find(pageNumber) != TLB.end()){
        frameNumber = TLB[pageNumber];
        TLB_Hits++;
    }
    
    if (frameNumber == -1)
        if(pageTable.find(pageNumber) != pageTable.end())
            frameNumber = pageTable[pageNumber];

    
    if (frameNumber == -1){
        frameNumber = availableFrame;
        readFromBackingStore(pageNumber);
        pageFault++;
    }
    addToTLB(pageNumber, frameNumber);
    int value = physicalMemory[frameNumber][offset];

    string s = ((frameNumber << 8) < 10000) ? "\t" : "";
    if (printAddress)
        cout
        << "Virtual address : " << to_string(logicalAddress) << "\t\t" 
        << "Physical address : " << to_string(frameNumber << 8) << "\t" << s
        << "Value : " << to_string(value) << endl;
}

int main(int argc, char* argv[]){

    int numberOfAddress = 0;

    if (argc != 2){
        cout << "Input Error ! " << endl << "Usage : Chap09 [input file]" << endl;
        exit(1);
    }
    backingStore.open("./data/BACKING_STORE.bin", ios::binary);
    ifstream addressFile(argv[1]);
    if (!addressFile.is_open()){
        cout << "Opening file error !" << endl;
        exit(1);
    }

    cout << "Do you wanna to print all the address ? [y/n] : ";
    char ans;
    cin >> ans;
    printAddress = (ans == 'y') ? true : false;
    if (printAddress)
        cout << "==================================================================" << endl;
    string buffer;
    int logicalAddress;
    while (getline(addressFile, buffer)){
        logicalAddress = atoi(buffer.c_str());
        getPage(logicalAddress);
        numberOfAddress++;
    }
    if (printAddress)
        cout << "==================================================================" << endl;

    cout << "TLB Hits : " << TLB_Hits << endl;
    cout << "Page fault : " << pageFault << endl;


   return 0;
}