using namespace std;

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <thread>
#include <random>
#include <ctime>
#include <mutex>
#include <unistd.h>
#include <algorithm>

int* totalResource;
int* currentResource;
bool* processRunning;
int totalResourceCount;
int processCount;
int totalCustomer;
string* currentResourceString;
mutex mtx;

bool checkSafety(int* requestRes){
    for(int i = 0; i < totalResourceCount; i++)
        if (currentResource[i] < requestRes[i])
            return false;
    return true;
}

bool getResourceFromBank(int id, int* requestRes){
    mtx.lock();
    if (checkSafety(requestRes) == true){
        processRunning[id] = true;
        string result = "";
        for(int i = 0; i < totalResourceCount; i++){
            currentResource[i] -= requestRes[i];
            result += to_string(currentResource[i]) + " ";
        }
        currentResourceString[id] = result;
        mtx.unlock();
        return true;
    }
    if (count(processRunning, processRunning + totalCustomer, true) == 0)
        exit(1);
    mtx.unlock();
    return false;
}

void releaseResourceFromBank(int* res){
    mtx.lock();
    for(int i = 0; i < totalResourceCount; i++)
        currentResource[i] += res[i];
    mtx.unlock();
}

void createCustomer(int id){
    while(true){
        string s = "";
        int* requestResource = new int[totalResourceCount];
        s += "Customer " + to_string(id) + " : request ";
        for (int i = 0; i < totalResourceCount; i++){
            requestResource[i] = rand() % totalResource[i];
            s += (to_string(requestResource[i]) + " ");
        }
        s += '\n';
        cout << s;
        bool request = false;
        while(!request){
            sleep(1);
            request = getResourceFromBank(id, requestResource);
        }
        s = "";
        s += "Customer " + to_string(id) + " : There are ";
        s += currentResourceString[id];
        s += "left\n\n";
        cout << s;
        sleep(1);
        releaseResourceFromBank(requestResource);
        delete[] requestResource;
    }
}

void stringToTotalResouce(string input){
    stringstream stream(input);
    char c;
    vector<int> res;
    stream >> c;
    do{
        int buffer;
        stream >> buffer >> c;
        res.push_back(buffer);
    } while (c == ',');
    totalResourceCount = res.size();
    totalResource = new int[totalResourceCount];
    currentResource = new int[totalResourceCount];
    copy(res.begin(), res.end(), totalResource);
    copy(res.begin(), res.end(), currentResource);
}

int main(int argc, char* argv[]){
    string inputTotalResouce;
    srand(time(NULL));
    vector<thread> totalThread;
    processCount = 0;
    if (argc == 1){
        cout << "Enter the number of customer to create : ";
        cin >> totalCustomer;
        cin.ignore();
        cout << "Enter the number of total resource by the following format \"[r1, r2, ..., rn]\" : ";
        getline(cin, inputTotalResouce);
    } else if (argc == 3){
        totalCustomer = atoi(argv[1]);
        inputTotalResouce = argv[2];
    }
    processRunning = new bool[totalCustomer];
    currentResourceString = new string[totalCustomer];
    
    stringToTotalResouce(inputTotalResouce);
    
    for (int i = 0; i < totalCustomer; i++){
        processRunning[i] = false;
        totalThread.push_back(thread(createCustomer, i));
    }
    for (thread &t : totalThread)
        if (t.joinable())
            t.join();

    return 0;

}

